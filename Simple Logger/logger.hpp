//
//  logger.hpp
//  Simple Logger
//
//  Created by Drink on 9/6/2562 BE.
//  Copyright © 2562 Drink. All rights reserved.
//

#ifndef logger_hpp
#define logger_hpp

#define error(message) _error(message, __FUNCTION__, __FILE__, __LINE__)

#include <fstream>
#include <string>
#include <vector>

#include "json.hpp"

namespace simple_logger {

    const std::string DEFAULT_LOG_FILE_LOCATION = "./my_log_file.log";
    const std::string DEFAULT_TAG_NAME = "INFO";
    const std::string ERROR_TAG_NAME = "ERROR";
    const std::string WARNING_TAG_NAME = "WARNING";

    class Logger{

    public:
        static Logger* instance();

        void log(const std::string& tag, const std::string& message);
        void log(const std::string& message);
        void warning(const std::string& message);
        void _error(const std::string& message,
                    const std::string& function,
                    const std::string& file,
                    const int line );

        void load_config(std::string filename);

    private:
        Logger();
        ~Logger();

        void m_write_message_to_console(const std::string& tag, const std::string& message);
        void m_write_message_to_file(const std::string& tag, const std::string& message);


        // Copy Constructor is not supported
        Logger(const Logger& other);
        // Copy assignment operator is not supported
        Logger& operator=(Logger& other);

        static Logger* m_instance;

        std::ofstream m_file;
        nlohmann::json m_config;
    };



}

#endif /* logger_hpp */
