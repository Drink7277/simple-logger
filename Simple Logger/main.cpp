//
//  main.cpp
//  Simple Logger
//
//  Created by Drink on 9/6/2562 BE.
//  Copyright © 2562 Drink. All rights reserved.
//

#define CATCH_CONFIG_MAIN

#include <iostream>
#include <string>
#include <stdio.h>

#include "logger.hpp"
#include "catch.hpp" // Unit test Library

using namespace std;
using namespace simple_logger;

//int main(int argc, const char * argv[]) {
//
//    const string config_file = "/Users/drink/ITGT533/Simple Logger/Simple Logger/config.json";
//
//    Logger* logger = Logger::instance();
//
//
//    logger->load_config(config_file);
//
//    logger->log("This is my log");
//    logger->warning("This is error log");
//    logger->error("This is error log");
//
//    return 0;
//}

TEST_CASE("Logger can log any message to a file") {
    Logger* logger = Logger::instance();
    ifstream file(DEFAULT_LOG_FILE_LOCATION);

    SECTION("It can write INFO, WARNING and ERROR tags to file"){
        logger->log("This is INFO tag");
        logger->warning("This is WARNING tag");
        logger->error("This is ERROR tag");

        if (file.is_open())
        {
            string line;
            getline(file,line);
            REQUIRE(line == "[INFO]: This is INFO tag");

            getline(file,line);
            REQUIRE(line == "[WARNING]: This is WARNING tag");

            getline(file,line);
            REQUIRE(line == "[ERROR]: This is ERROR tag");

            getline(file,line);
            REQUIRE(line.find("Calling: ____C_A_T_C_H____T_E_S_T____") != string::npos);

            getline(file,line);
            REQUIRE(line.find(__FILE__) != string::npos);

            remove(DEFAULT_LOG_FILE_LOCATION.c_str());
        }
    }
}

TEST_CASE("Logger uses the Singleton pattern") {
    Logger* logger = Logger::instance();

    SECTION("New Instance will point to the same address") {
        REQUIRE( logger == Logger::instance() );
    }
}


TEST_CASE("Logger can load config from JSON file.") {
    const string config_file =  "/Users/drink/ITGT533/Simple Logger/Simple Logger/config.json";
    Logger* logger = Logger::instance();

    //Test that load_config can be called without error
    logger->load_config(config_file);
}
