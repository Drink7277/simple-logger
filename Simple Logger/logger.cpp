//
//  logger.cpp
//  Simple Logger
//
//  Created by Drink on 9/6/2562 BE.
//  Copyright © 2562 Drink. All rights reserved.
//

#include "logger.hpp"

#include <iostream>

using json = nlohmann::json;

namespace simple_logger {

    Logger* Logger::m_instance = nullptr;

    Logger* Logger::instance(){
        if(m_instance == nullptr) { m_instance = new Logger(); }
        assert(m_instance != nullptr);
        return m_instance;
    }

    void Logger::log(const std::string& tag, const std::string& message) {
        // Do nothing for empty log or empty tag
        if( message.empty() || tag.empty() ) { return; }

        assert(!message.empty());
        assert(!tag.empty());

        m_write_message_to_file(tag, message);
        m_write_message_to_console(tag, message);

        assert(m_file.is_open());
    }

    void Logger::log(const std::string& message) {
        log(DEFAULT_TAG_NAME, message);
    }

    void Logger::warning(const std::string& message) {
        log(WARNING_TAG_NAME, message);
    }

    void Logger::_error(const std::string& message,
                const std::string& function,
                const std::string& file,
                const int line ) {

        std::string new_message =
            message + '\n' +
            "Calling: " + function + '\n' +
            "At: " + file + ':' + std::to_string(line);

        log(ERROR_TAG_NAME, new_message);
    }

    void Logger::load_config(std::string filename){
        assert(!filename.empty());

        std::ifstream file (filename);
        if (file.is_open())
        {
            m_config = json::parse(file)["logger"];

            file.close();
        } else {
            throw "cannot read file";
        }
    }


    Logger::Logger() {
        m_file.open(DEFAULT_LOG_FILE_LOCATION, std::ios::app );
        // Default writ INFO WARNING ERROR to both file and console
        m_config = "{\"tags_to_console\":[\"INFO\",\"WARNING\",\"ERROR\"],\"tags_to_file\":[\"INFO\",\"WARNING\",\"ERROR\"]}"_json;
    }

    Logger::~Logger() {
        m_file.close();
    }

    void Logger::m_write_message_to_console(const std::string& tag, const std::string& message){

        auto tags_to_console = m_config["tags_to_console"];

        // return if tag is not match tags_to_console
        if (std::find(tags_to_console.begin(), tags_to_console.end(), tag) == tags_to_console.end()) { return; }

        std::cout << '[' << tag << "]: " << message << std::endl;
    }

    void Logger::m_write_message_to_file(const std::string& tag, const std::string& message){

        auto tags_to_file = m_config["tags_to_file"];

        // return if tag is not match tags_to_file
        if (std::find(tags_to_file.begin(), tags_to_file.end(), tag) == tags_to_file.end()) { return; }

        m_file << '[' << tag << "]: " << message << std::endl;
    }


    Logger::Logger(const Logger& other) {
        throw "Copy Constructor is not supported";
    }

    Logger& Logger::operator=(Logger& other) {
        throw "Copy assignment operator is not supported";
    }



}
